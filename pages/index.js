function updateCaptcha() {
    var image = document.getElementById('captcha-image');
    var captchaInput = document.getElementById('captchaId');
    
    var arr = [1,2,3,4,5];


    if (!image || !captchaInput) return;

    image.src = 'https://test1.aws.qlearsite.com/app/ws/captcha?captchaId=' + captchaInput.value + '&timestamp=' + Date.now();
}

(function() {
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        var tooltips = document.querySelectorAll('[data-tooltip]');

        function closeAllTooltips() {
            if (tooltips && tooltips.length) {
                [].forEach.call(tooltips, function(item) { item.classList.remove('tooltip-open'); });
            }

            window.removeEventListener('touchstart', closeAllTooltips);
        }

        if (tooltips && tooltips.length) {
            function tooltipHandler(tooltip) {
                tooltip.classList.add('tooltip-open');

                window.addEventListener('touchstart', closeAllTooltips);
            }

            [].forEach.call(tooltips, function(tooltip) {
                tooltip.addEventListener('touchstart', function(e) {
                    e.stopPropagation();
                    tooltipHandler(tooltip);
                });
            });
        }
    }
})();