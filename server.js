var express = require('express');

var app = express();

app.use(express.static('pages'));

app.get('/', (req, res) => {
    res.sendFile(__dirname +  '/pages/test.html')
});

app.listen(2000, () => console.log('Server works on http://localhost:2000'));


// http.createServer((req, res) => {
//
//     req.addListener('end', function () {
//         //
//         // Serve files!
//         //
//         fileServer.serve(req, res);
//     }).resume();

    // switch (req.url) {
        // case '/login.html':
        //     res.writeHead(200, {'Content-Type': 'text/html'});
        //     fs.createReadStream('./pages/login.html').pipe(res);
        //     break;
        // case '/styles/page.css':
        //     res.writeHead(200, {'Content-Type': 'text/css'});
        //     fs.createReadStream('./styles/page.css').pipe(res);
        //     break;
        // default:
        //     res.writeHead(200, {'Content-Type': 'text/html'});
        //     fs.createReadStream('./pages/test.html').pipe(res);
    // }
// }).listen(2000, () => 'Server works on http://localhost:2000');